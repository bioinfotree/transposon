#!/usr/bin/env python

# get_coverage_from_sams.py --- 
# 
# Filename: get_coverage_from_sams.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Mon Apr  4 18:33:28 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# argument parser
import argparse
import sys
import os
from SAMTOOLS import SAMTOOLS
import shutil




def main():
        tmp_dir = str()
	stat_file_name = str()
	empty_file_name = str()

	# parse command line arguments
	args = arg_parse()
	if args.force:
		shutil.rmtree(args.tmp_dir)
	if os.path.isdir(args.tmp_dir):
		raise SystemError("%s in not empty!" % args.tmp_dir)		

	
  	samtools = SAMTOOLS()
	samtools.set_assembly(os.path.dirname(args.reference), os.path.basename(args.reference))
	samtools.set_alignments_path(args.sam_dir)
	tmp_dir = samtools.set_tmp_dir(args.tmp_dir)
	stat_file_name, empty_file_name = samtools.get_coverage_stats(unique=args.unique)
	
	return 0
	

def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Use samtools pileup to get average coverage informations from one or more SAM files. \
    SAM files must be collected on a common directory. \
    The reference sequences of each SAM file must be collected in a single FASTA file. \
    Result files are stored on a new temporary directory. Error if the directory already exists. \
    SAM files empty or without a reference are listed in *_empty.txt file.", prog=sys.argv[0])
    parser.add_argument("--version", "-v", action="version", version="%(prog)s 0.1")
    parser.add_argument("--reference", "-r", dest="reference", required=True, help="Reference file in FASTA format")
    parser.add_argument("--sam_dir", "-d", dest="sam_dir", required=True, help="Directory containing SAM files.")
    parser.add_argument("--tmp_dir", "-t", dest="tmp_dir", required=False, default="tmp", help="Temporary directory for result files. Default tmp.")
    parser.add_argument("--force", "-f", dest="force", required=False, action="store_true", default=False, help="Force removal existing temporary directory.")
    parser.add_argument("--unique", "-u", dest="unique", required=False, action="store_true", default=False, help="retain only alignments with mapping quality >=1 (uniquely mapped read)")

    args = parser.parse_args()
    
    return args




# execute the main function
if __name__ == "__main__":
    main()

# 
# get_coverage_from_sams.py ends here
