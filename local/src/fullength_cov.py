#!/usr/bin/env python
from __future__ import with_statement

import sys
import argparse
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage



def main():
	args = arg_parse()

	ref_len_handle = open(args.reference, "r")

	ref_len = int(0)
	with sys.stdin as stdin:
		line = stdin.readline()
		tokens = safe_rstrip(line).split('\t')
		assert len(tokens) == 3
		ref_name, pos, cov = tokens
		ref_len = find_len(ref_len_handle, ref_name)



#sys.stout.write("%s\t%i\t0\n" %(ref_name, n))
					
				
			
				
			

	ref_len_handle.close()



def find_len(ref_len_handle, target_name):
	found = False
	while not found:
		line = ref_len_handle.readline()
		if not line:
			break
		ref_name, length = safe_rstrip(line).split("\t")
		if ref_name == target_name:
			return int(length)
	return 0



def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="", prog=sys.argv[0])
    parser.add_argument("--version", "-v", action="version", version="%(prog)s 0.1")
    parser.add_argument("--reference", "-r", dest="reference", required=True, help="")
    args = parser.parse_args()
    
    return args


if __name__ == '__main__':
	main()

