### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# consider to add BOWTIE 2 aligner

CPUS ?= 4
#
TEST ?= true
#
REFERENCE_1 ?=
#
REFERENCE_2 ?=
#
READS_FASTA ?=
#
READS_QUAL ?=
#
ALIGNER ?= ssaha2

# for using fastaqual2fastq
context prj/454_preprocessing


# create dirs
log:
	mkdir -p $@;


reference_1.fasta: $(REFERENCE_1)
	fastatool sanitize $< $@



# transform to reads to fastq sanger
reads.fastq: $(READS_FASTA) $(READS_QUAL)
	fastaqual2fastq --fasta $< --qual $^2 --hash_sort > $@



# choose aligner
ifeq ($(ALIGNER),ssaha2)
########## SSAHA2 #########

# create the hash table
reference_1_ssaha2_db.base: reference_1.fasta
	ssaha2Build -454 -save $(basename $@) $<


# align on reference 1
reference_1_mapped.sam: reference_1_ssaha2_db.base reads.fastq log
	ssaha2 -454 -output sam_soft -best -1 -outfile $@ -save $(basename $<) $^2 &> ./$^3/ssaha2.reference_1.log

endif




ifeq ($(ALIGNER),bwasw)
########## BWASW #########

# indexes the referece 1
# use IS algorithm due to short reference
reference_1_bwa_db.bwt: reference_1.fasta
	bwa index -p $(basename $@) -a is $<


# align on reference 1
reference_1_mapped.sam: reference_1_bwa_db.bwt reads.fastq log
	bwa bwasw -t $(CPUS) $(basename $<) $^2 > $@ 2> ./$^3/bwasw.reference_1.log

endif


# reference_1_mapped.cov: sams reference_1.fasta
# 	samtools view -bq 1 -T $^2 ./$</reference_1_mapped.sam | \
# 	samtools sort -o - - | \
# 	genomeCoverageBed -ibam stdin -d > (script x calcolo_media) $@


reference_1.fasta_stats.txt: reference_1.fasta reference_1_mapped.sam
	COV=cov; \
	sam_coverage --reference $< --sam_dir . --tmp_dir $$COV --unique && \
	ln -sf ./$$COV/$@



.PHONY: test
test:
	echo $(STD_ERR)



# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += log \
	 reference_1.fasta \
	 reads.fastq \
	 reference_1_mapped.sam \
	 reference_1.fasta_stats.txt


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += reference_1.fasta \
	 reads.fastq \
	 reference_1_bwa_db \
	 reference_1_mapped.sam \
	 reference_1.fasta.fai \
	 reference_1.fasta_stats.txt


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf log
	$(RM) -rf cov
	$(RM) -f reference_1_bwa_db.*
	$(RM) -f reference_1_ssaha2_db.*

######################################################################
### phase_1.mk ends here
