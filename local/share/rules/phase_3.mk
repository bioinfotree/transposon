### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


# for using fastaqual2fastq
context prj/454_preprocessing


CPUS ?= 4
#
TEST ?= true
#
REFERENCE_1 ?=
#
REFERENCE_2 ?=
#
READS_FASTA ?=
#
READS_QUAL ?=
#
ALIGNER ?= ssaha2



ifeq ($(ASSEMBLER),m)
extern ../phase_2/trasp_assembly/trasp_d_results/trasp_out.unpadded.fasta as ASS_FASTA
extern ../phase_2/trasp_assembly/trasp_d_results/trasp_out.unpadded.fasta.qual as ASS_QUAL
endif

ifeq ($(ASSEMBLER),n)
extern ../phase_2/trasp_assembly/assembly/454Isotigs.fna as ASS_FASTA
extern ../phase_2/trasp_assembly/assembly/454Isotigs.qual as ASS_QUAL
endif


# create dirs
log:
	mkdir -p $@;





reference_2.fasta: $(REFERENCE_2)
	fastatool sanitize $< $@

# transform to reads to fastq sanger
assembly.fastq: $(ASS_FASTA) $(ASS_QUAL)
	fastaqual2fastq --fasta $< --qual $^2 > $@





# choose aligner
ifeq ($(ALIGNER),ssaha2)
########## SSAHA2 #########

# create the hash table
reference_2_ssaha2_db.base: reference_2.fasta
	ssaha2Build -454 -save $(basename $@) $<


# align on reference 1
reference_2_mapped.sam: reference_2_ssaha2_db.base assembly.fastq log
	ssaha2 -454 -output sam_soft -outfile $@ -save $(basename $<) $^2 &> ./$^3/ssaha2.reference_2.log
	#ssaha2 -454 -output sam_soft -best -1 -outfile $@ -save $(basename $<) $^2 &> ./$^3/ssaha2.reference_2.log

endif


ifeq ($(ALIGNER),bwasw)
########## BWASW #########

# indexes the referece 1
# use IS algorithm due to short reference
reference_2_bwa_db.bwt: reference_2.fasta
	bwa index -p $(basename $@) -a is $<


# align on reference 1
reference_2_mapped.sam: reference_2_bwa_db.bwt assembly.fastq log
	bwa bwasw -t $(CPUS) $(basename $<) $^2 > $@ 2> ./$^3/bwasw.reference_2.log

endif




reference_2.fasta_stats.txt: reference_2.fasta reference_2_mapped.sam
	COV=cov; \
	sam_coverage --reference $< --sam_dir . --tmp_dir $$COV --unique && \
	ln -sf ./$$COV/$@




.PHONY: test
test:
	echo $(STD_ERR)



# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += reference_2_mapped.sam \
	 reference_2.fasta_stats.txt

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += reference_2.fasta \
	 assembly.fastq \
	 reference_2.fasta.fai \
	 reference_2_mapped.sam \
	 reference_2.fasta_stats.txt

# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf log
	$(RM) -rf cov
	$(RM) -f reference_2_ssaha2_db.*
	$(RM) -f reference_2_bwa_db.*

######################################################################
### phase_1.mk ends here
