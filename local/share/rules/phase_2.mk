### phase_2.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# consider to add BOWTIE 2 aligner

context prj/radtools
context prj/454_preprocessing

CPUS ?= 4
#
TEST ?= true
#
REFERENCE_1 ?=
#
REFERENCE_2 ?=
#
READS_FASTA ?=
#
READS_QUAL ?=
#
ALIGNER ?= ssaha2
#
ASSEMBLER ?= n

extern ../phase_1/reference_1_mapped.sam as REF1SAM
extern ../phase_1/reference_1.fasta as REF1


# create dirs
log:
	mkdir -p $@;



reference_1_ln.fasta: $(REF1)
	ln -sf $< $@

reference_1_ln.fasta.fai: reference_1_ln.fasta
	samtools faidx $<

# q1 options to filter for aligned reads
reference_1_mapped.bam: $(REF1SAM) reference_1_ln.fasta.fai
	samtools view -uq1 -t $^2 $< | samtools sort -o - - > $@


mapped_reads.fastq: reference_1_mapped.bam
	bam2fastq --output $@ --force --aligned --no-unaligned --strict $<


unmapped_reads.fastq: reference_1_mapped.bam
	bam2fastq --output $@ --force --no-aligned --unaligned --strict $<


reference_1.qual: reference_1_ln.fasta
	dummy_qual_from_fasta --fasta $< --start 50 --end 50 > $@


reference_1.fastq: reference_1_ln.fasta reference_1.qual
	fastaqual2fastq --fasta $< --qual $^2 > $@


ref-mapped_reads.fastq: reference_1.fastq mapped_reads.fastq
	cat $< $^2 > $@




ifeq ($(ASSEMBLER),m)
# use mira assembler
trasp_assembly: mapped_reads.fastq log reference_1_ln.fasta
	mira -job=denovo,est,accurate,454 --fastq --notraceinfo -project=$(subst _assembly,,$@) COMMON_SETTINGS -GE:not=4 -LR:fo=no -OUT:rtd=yes -SB:lb=yes:bft=fasta:bbq=40 -FN:bbin=$^3 454_SETTINGS -FN:fqi=$< -CL:cpat=0:qc=0 -ED:ace=1 -OUT:sssip=yes -CO:fnicpst=yes -LR:mxti=no -AS:mrpc=2 &> ./$^2/mira.$(basename $^1).log

#mira -job=denovo,est,accurate,454 --fastq --notraceinfo -project=$(subst _assembly,,$@) COMMON_SETTINGS -GE:not=4 -LR:fo=no -OUT:rtd=yes -SB:lb=yes:bft=fasta:bbq=40 -FN:bbin=$^3 454_SETTINGS -FN:fqi=$< -CL:cpat=0:qc=0 -ED:ace=1 -OUT:sssip=yes -CO:fnicpst=yes -LR:mxti=no -AS:mrpc=2 -AL:ms=1:mrs=55 &> ./$^2/mira.$(basename $^1).log

#mira -job=denovo,est,accurate,454 --fastq --notraceinfo -project=$(subst _assembly,,$@) COMMON_SETTINGS -GE:not=4 -LR:fo=no -OUT:rtd=yes -SB:lb=yes:bft=fasta:bbq=40 -FN:bbin=$^3 454_SETTINGS -FN:fqi=$< -CL:cpat=0:qc=0 -ED:ace=1 -OUT:sssip=yes -CO:fnicpst=yes -LR:mxti=no -AS:mrpc=1 -AL:mo=5 &> ./$^2/mira.$(basename $^1).log

#mira -job=denovo,est,accurate,454 --fastq --notraceinfo -project=$(subst _assembly,,$@) COMMON_SETTINGS -GE:not=4 -LR:fo=no -OUT:rtd=yes 454_SETTINGS -FN:fqi=$< -CL:cpat=0:qc=0 -ED:ace=1 -OUT:sssip=yes -CO:fnicpst=yes -LR:mxti=no -AS:mrpc=1 -AL:mo=5 &> ./$^2/mira.$(basename $^1).log


endif

ifeq ($(ASSEMBLER),n)
trasp_assembly: ref-mapped_reads.fastq log reference_1_ln.fasta
# use newbler assembler
	newAssembly -cdna $@ && \
	addRun $@ $< && \
	runProject -cpu $(CPUS) -novs -nov -novt -notrim -ace -urt -het $@ &> ./$^2/newbler.$(basename $^1).log


endif



.PHONY: test
test:
	echo $(subst _assembly,,transp_assembly)



# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += log \
	 mapped_reads.fastq \
	 trasp_assembly

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += reference_1_ln.fasta \
	 reference_1_ln.fasta.fai \
	 reference_1_mapped.bam \
	 mapped_reads.fastq \
	 unmapped_reads.fastq \
	 reference_1.qual \
	 reference_1.fastq \
	 ref-mapped_reads.fastq



# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf log
	$(RM) -rf trasp_assembly

######################################################################
### phase_2.mk ends here
